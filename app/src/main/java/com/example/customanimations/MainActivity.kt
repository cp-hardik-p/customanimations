package com.example.customanimations

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.*
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.customanimations.ui.theme.CustomanimationsTheme
import com.example.customanimations.ui.theme.ThemeColor

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CustomanimationsTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = ThemeColor
                ) {
                    CustomAnimations()
                }
            }
        }
    }
}


@Composable
fun CustomAnimations() {
    val scrollState = rememberScrollState()
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier.verticalScroll(scrollState)
    ) {
        val infiniteTransition = rememberInfiniteTransition()
        HeartbeatAnimation(infiniteTransition)
        WiggleAnimation(infiniteTransition)
        FlashAnimation(infiniteTransition)
        ChasingCircleAnimation(infiniteTransition)
    }
}

@Composable
fun HeartbeatAnimation(infiniteTransition: InfiniteTransition) {
    val heartbeatAnimation by infiniteTransition.animateFloat(
        initialValue = 1f,
        targetValue = 1.4f,
        animationSpec = infiniteRepeatable(
            animation = tween(1000),
            repeatMode = RepeatMode.Reverse
        )
    )

    Image(
        modifier = Modifier
            .scale(heartbeatAnimation)
            .size(125.dp)
            .padding(top = 28.dp),
        painter = painterResource(id = R.drawable.ic_heart_black),
        contentDescription = "",
        colorFilter = ColorFilter.tint(Color.White)
    )
}

@Composable
fun WiggleAnimation(infiniteTransition: InfiniteTransition) {
    val angleOffset = 30f
    val wiggleAnimation by infiniteTransition.animateFloat(
        initialValue = -angleOffset,
        targetValue = angleOffset,
        animationSpec = infiniteRepeatable(
            animation = tween(500, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Box(
        modifier = Modifier
            .padding(top = 60.dp)
            .size(125.dp)
            .rotate(wiggleAnimation)
            .clip(RoundedCornerShape(12.dp))
            .background(Color.White)
    )
}

@Composable
fun FlashAnimation(infiniteTransition: InfiniteTransition) {
    val flashAnimation by infiniteTransition.animateFloat(
        initialValue = 1f,
        targetValue = 0f,
        animationSpec = infiniteRepeatable(
            tween(1000, easing = FastOutSlowInEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Box(
        modifier = Modifier
            .padding(top = 60.dp)
            .size(125.dp)
            .clip(RoundedCornerShape(12.dp))
            .background(Color.White.copy(flashAnimation))
    )
}

@SuppressLint("UnrememberedAnimatable", "CoroutineCreationDuringComposition")
@Composable
fun ChasingCircleAnimation(infiniteTransition: InfiniteTransition) {
    val arcAngle1 by infiniteTransition.animateFloat(
        initialValue = 0F,
        targetValue = 180F,
        animationSpec = infiniteRepeatable(
            animation = tween(1000, easing = LinearEasing),
            repeatMode = RepeatMode.Restart
        )
    )

    val arcAngle2 by infiniteTransition.animateFloat(
        initialValue = 180F,
        targetValue = 360F,
        animationSpec = infiniteRepeatable(
            animation = tween(1000, easing = LinearEasing),
            repeatMode = RepeatMode.Restart
        )
    )

    val scaleCircles by infiniteTransition.animateFloat(
        initialValue = 0.3f,
        targetValue = 1f,
        animationSpec = infiniteRepeatable(
            animation = tween(2000),
            repeatMode = RepeatMode.Reverse
        )
    )

    Box(
        Modifier
            .padding(top = 28.dp)
            .scale(scaleCircles)
    ) {
        Box(
            Modifier
                .align(Alignment.Center)
                .size(50.dp)
                .background(Color.White, shape = CircleShape)
        ) {
            Canvas(modifier = Modifier
                .rotate(arcAngle1)
                .align(Alignment.Center)
                .size(30.dp), onDraw = {
                drawArc(
                    color =
                    Color.Black,
                    style = Stroke(
                        width = 10f,
                        cap = StrokeCap.Round
                    ),
                    startAngle = 180f,
                    sweepAngle = 288f,
                    useCenter = false
                )

            })
        }

        Box(Modifier.rotate(arcAngle2)) {
            Canvas(modifier = Modifier
                .rotate(180f)
                .align(Alignment.Center)
                .size(100.dp), onDraw = {
                drawArc(
                    color =
                    Color.White,
                    style = Stroke(
                        width = 10f,
                        cap = StrokeCap.Round
                    ),
                    startAngle = 180f,
                    sweepAngle = 288f,
                    useCenter = false
                )
            }
            )
        }

        Canvas(modifier = Modifier
            .rotate(arcAngle2)
            .align(Alignment.Center)
            .size(60.dp), onDraw = {
            drawArc(
                color =
                Color.White,
                style = Stroke(
                    width = 10f,
                    cap = StrokeCap.Round
                ),
                startAngle = 180f,
                sweepAngle = 288f,
                useCenter = false
            )
        }
        )
    }
}



